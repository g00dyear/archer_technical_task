Magazine REST API
==============

#Installation
--------------
**Create virtualenv**

mkvirtualenv env -p python3.5

**Install project requirements**

pip install -r requirements.txt

Running
--------------
**Migrate database**

./manage.py migrate

**Load fixtures**

./manage.py loaddata fixtures/data.json

**Run application**

./manage.py runserver


##URL desctiption:
---------------

##Create user

**Method**

POST /magazine/v1/register/

**Parameters**

Header parameters:

 NAME  | Mandatory | Description  
------------ | ------------- | -------
Content-Type |  Yes  | Data exchange takes place by means of json messaging

Body parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    email   |      Yes      |Required params for registration
  password  |      Yes      |Required params for registration
    group   |      Yes      |Required params for registration             

Response Codes:

  Code   |                      Description                      
-------- | ------------------------------------------------------
   201   | User successfully created    
   422   | {field_name} is required                                      
   400   | User already exists                 

**Request example**

```json
{
	"email":"example@example.com",
	"password": "hereIsYourPassword",
	"group": "editor"
}
```
**Response example**

```json
{
 	"token": "d8ebee0135d338caa1511ff6a68a70aa4eb3c5fa"
}
```

##Login user

**Method**

POST /magazine/v1/login

**Parameters**

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type|      Yes      |  Data exchange takes place by means of json messaging


Body parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    email   |      Yes      |Required param for login 
  password  |      Yes      |Required param for login


Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   201   | Successfully logged in     
   400   | Credentials are incorrect            

**Request example**

```json
{
	"email":"example@example.com",
	"password": "hereIsYourPassword"
}
```
**Response example**

```json
{
 	"token": "d8ebee0135d338caa1511ff6a68a70aa4eb3c5fa"
}
```

##Create post

**Method**

POST /magazine/v1/posts

**Parameters**

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type |      Yes      |Data exchange takes place by means of json messaging
Authorization|      Yes      |Token is required

Boady parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    title   |      Yes      |One of required params for posting  |
    body    |      Yes      |One of required params for posting  |

Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   201   |           Post has been created successfuly           
   400   |            {field_name} is required field             

**Request example**

```json
{
	"title": "Title",
	"body": "Text here"
}
```
**Response example**

```json
{
  "id": 4,
  "title": "Title",
  "body": "Text here",
  "is_published": false
}
```

##Publish post

**Method**

PUT /magazine/v1/posts/publish/{id}

**Parameters**

Header parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
Content-Type |      Yes      | Data exchange takes place by means of json messaging
Authorization|      Yes      |Token is required

Boady parameters:

 NAME  | Is Mandatory? | Description  
------------ | ------------- | -------
    title   |      Yes      |One of required params for posting  
    body    |      Yes      |One of required params              

Response Codes:

  Code   |                      Description                      |
---------|-------------------------------------------------------|
   201   |           Post has been created successfuly           
   400   |            {field_name} is required field             
   403   |                        Invalid token                  
   404   |                        Not found                      

Request example

```json
{
	"title": "Title",
	"body": "Text is here",
	"is_published": false
}
```
Response example

```json
{
  "id": 5,
  "title": "Title",
  "body": "Text is here",
  "is_published": true
}
```

##Profile details

**Method**

GET /magazine/v1/profile

**Parameters**

Response Codes:

  Code   |                      Description                      
---------|-------------------------------------------------------
   200   |                          OK                           

**Request example**

/magazine/v1/profile

**Response example**

```json
{
  "id": 2,
  "email": "example@example.com"
}
```


##Get all posts

GET /magazine/v1/posts?size={size}&page_size={page_size}

Parameters

Response Codes:

  Code   |                      Description                      |
---------|-------------------------------------------------------|
   200   |                          OK                           

Response example

```json
	{
	  "count": 7,
	  "next": "http://localhost:8000/magazine/v1/posts?page=2&page_size=5&size=4",
	  "previous": "http://localhost:8000/magazine/v1/posts?page=1&page_size=5&size=4",
	  "results": [
	    {
	      "id": 1,
	      "title": "Title1",
	      "body": "Body1",
	      "is_published": false
	    },
	    {
	      "id": 2,
	      "title": "Title2",
	      "body": "Body2",
	      "is_published": true
	    },
	    {
	      "id": 3,
	      "title": "Title3",
	      "body": "Body3",
	      "is_published": true
	    }
	  ]
}
```

