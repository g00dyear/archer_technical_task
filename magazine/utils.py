from magazine.serializers import PostSerializer

from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter


class Pagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000


class PostView:
    filter_backends = (SearchFilter,)
    search_fields = ('title', 'body')
    serializer_class = PostSerializer
    pagination_class = Pagination
