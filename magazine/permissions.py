from rest_framework.permissions import BasePermission


class IsEditor(BasePermission):
    def has_permission(self, request, view):
        is_auth = request.user and request.user.is_authenticated()
        return is_auth and request.user.has_perm('magazine.status_change')
