from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.status import HTTP_422_UNPROCESSABLE_ENTITY
from rest_framework.validators import UniqueValidator
from django.utils.translation import ugettext_lazy as _

from magazine.models import Post


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    group = serializers.ReadOnlyField()
    email = serializers.EmailField(validators=[
        UniqueValidator(
            queryset=get_user_model().objects.all(),
            message="User with that email already exist",
        )]
    )

    class Meta:
        model = get_user_model()
        fields = '__all__'

    def validate(self, attrs):
        if not self.initial_data.get('group'):
            raise serializers.ValidationError(
                code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail={'group': '"group" is required field'}
            )
        if not Group.objects.filter(name=self.initial_data.get('group')).exists():
            raise serializers.ValidationError(
                code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail={'group': 'Group with name: "{}" does not exist'.format(
                    self.initial_data.get('group')
                )}
            )
        return super().validate(attrs)

    def create(self, attrs):
        user = get_user_model().objects.create_user(
            email=attrs.get('email'),
            password=attrs.get('password')
        )
        user.groups.add(Group.objects.get(name=self.initial_data.get('group')))
        return user


class AuthTokenEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        msg = None

        if email and password:
            user = authenticate(email=email, password=password)
            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
            else:
                msg = _('Unable to log in with provided credentials.')
        else:
            msg = _('Must include "email" and "password".')

        if msg:
            raise serializers.ValidationError(
                code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail=msg
            )
        attrs['user'] = user
        return attrs


class PublishedPostSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        if 'data' in kwargs:
            kwargs['data']['is_published'] = True
        else:
            kwargs['data'] = dict(is_published=True)
        super(PublishedPostSerializer, self).__init__(*args, **kwargs)

    class Meta:
        model = Post
        fields = '__all__'


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'email',)
