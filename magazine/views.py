from django.contrib.auth import get_user_model
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.views import APIView

from magazine.models import Post
from magazine.serializers import (
    UserSerializer,
    AuthTokenEmailSerializer,
    UserProfileSerializer,
    PublishedPostSerializer,
)
from magazine.permissions import IsEditor
from magazine.utils import PostView


class CreateUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            data={'token': serializer.instance.auth_token.key},
            status=HTTP_201_CREATED,
            headers=headers
        )


class LoginView(ObtainAuthToken):
    serializer_class = AuthTokenEmailSerializer


class CurrentUserView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        serializer = UserProfileSerializer(request.user)
        return Response(serializer.data)


class PostListView(PostView, ListCreateAPIView):
    queryset = Post.objects.all()
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)


class PublishedPostListView(PostView, ListAPIView):
    queryset = Post.objects.filter(is_published=True).all()


class PublishedPostView(UpdateAPIView):
    queryset = Post.objects.filter(is_published=False).all()
    serializer_class = PublishedPostSerializer
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsEditor,)
